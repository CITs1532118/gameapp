<?php
namespace Classes;

/**
 * class of cepter info
 */
class Cepter
{
    private $name;
    private $money;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}

$test = new Cepter('testCepter');

echo $test->getName();
